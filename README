Testing

Other than the given material in the email, I additionally tested the code with bad inputs (i.e. blanks), leading to the NULL checks after strtok. Otherwise, there could be the case where two NAT rules match, but I was unable to figure out which should have precedence. Right now the code defaults to first matching rule.

A basic edit to allow for testing is that if the program detects any arguments, it uses "-TEST" files rather than the original. This could be expanded to allow users to specify which files should be used.

Interesting

There's nothing too interesting about this code. It parses NAT information first, making (src_ip, src_port, outgoing) combinations. There's a bit of odd string processing to remove newlines passed in from the file, but it is not uncommon.

Refinements

Due to not coding in C very often, I'm a bit rusty on dealing with it. The malloc-strcpy in parse_nat may not be needed, though I was getting odd errors in the main function (maybe stack vs heap problem). Inherently, plain C lacks the data structures like maps/hashes that would allow me to search through NAT information faster than O(N) search, without me implementing it myself.

Note that in Testing and its NULL checks, it silently drops the information. This is partially from not figuring out what should be displayed to user, partially from strtok affecting the original string (and thus needing the overhead of malloc, strncpy, free ...).
