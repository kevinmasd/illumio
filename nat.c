#include <stdio.h>
#include <string.h>
#include <stdlib.h>

int MAX_LINE = 100;
int NAT_VALUES = 50;
char* NAT_FILE = "NAT";
char* FLOW_FILE = "FLOW";
char* OUTPUT_FILE = "OUTPUT";

typedef struct nat_pair {
    char* src_ip;
    char* src_port;
    char* outgoing;
} nat_pair;

int parse_nat(nat_pair* nat_info[]) {
    FILE* NAT = fopen(NAT_FILE, "r");
    if (NAT == NULL) {
        printf("NAT file cannot be opened");
        exit(1);
    }

    int nat_count = 0;
	
    char buf[MAX_LINE];
    nat_pair* pair;
    char* raw;
    while (!feof(NAT)) {
        fgets(buf, MAX_LINE, NAT);
        if (strlen(buf) <= 1) {
            continue;
        }
        pair = (nat_pair*) malloc(sizeof(nat_pair));
        
        raw = strtok(buf, ":");
        if (raw == NULL) {
            continue;
        }
        pair->src_ip = malloc(sizeof(char) * (strlen(raw) + 1));
        strncpy(pair->src_ip, raw, strlen(raw) + 1);

        raw = strtok(NULL, ",");
        if (raw == NULL) {
            continue;
        }
        pair->src_port = malloc(sizeof(char) * (strlen(raw) + 1));
        strncpy(pair->src_port, raw, strlen(raw) + 1);

        raw = strtok(NULL, ",");
        if (raw == NULL) {
            continue;
        }
        pair->outgoing = malloc(sizeof(char) * (strlen(raw) + 1));
        strncpy(pair->outgoing, raw, strlen(raw) + 1);

        nat_info[nat_count] = pair;
        nat_count++;
    }
    return nat_count;
}

int match(nat_pair* info, char* ip, char* port) {
    return ((strcmp(info->src_ip, "*") == 0) ||
            (strcmp(info->src_ip, ip) == 0)) &&
           ((strcmp(info->src_port, "*") == 0) ||
            (strcmp(info->src_port, port) == 0));
}


int main(int argc, char* argv[]) {
    if (argc > 1) {
        FLOW_FILE = "FLOW-TEST";
        NAT_FILE = "NAT-TEST";
        OUTPUT_FILE = "OUTPUT-TEST";
    }
    nat_pair* nat_info[NAT_VALUES];
    int nat_values = parse_nat(nat_info);
    if (nat_values == 0) {
	printf("NAT file empty");
	return 1;
    }

    FILE* FLOW = fopen(FLOW_FILE, "r");
    if (FLOW == NULL) {
	printf("Flow file cannot be opened");
	return 1;
    }

    FILE* OUTPUT = fopen(OUTPUT_FILE, "w");
    if (OUTPUT == NULL) {
	printf("Output file cannot be written");
	return 1;
    }

    char buf[MAX_LINE];
    char* ip;
    char* port;
    char* newline;
    int i;
    while (!feof(FLOW)) {
	fgets(buf, MAX_LINE, FLOW);
        if (strlen(buf) <= 1) {
            continue;
        }

	ip = strtok(buf, ":");
        if (ip == NULL) {
            continue;
        }

	port = strtok(NULL, ":");
        if (port == NULL) {
            continue;
        }

	if (strchr(port, '\n') != NULL) {  // Handles \n cleanup
	    newline = strchr(port, '\n');
	    *newline = '\0';
	}

	for (i = 0; i < nat_values; i++) {
	    nat_pair* info = nat_info[i];
	    if (match(info, ip, port) == 1) {
		fprintf(OUTPUT, "%s:%s -> %s", ip, port, info->outgoing);
                break;
	    }
	}

        if (i == nat_values) {
            fprintf(OUTPUT, "No nat match for %s:%s\n", ip, port); 
        }
    }

    for (i = 0; i < nat_values; i++) {
        free(nat_info[i]);
    }
}

